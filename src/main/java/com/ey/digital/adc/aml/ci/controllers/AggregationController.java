package com.ey.digital.adc.aml.ci.controllers;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.ci.model.Script;
import com.ey.digital.adc.aml.ci.model.TableSetting;
import com.ey.digital.adc.aml.ci.model.TestExecutionResults;
import com.ey.digital.adc.aml.ci.model.TestStatus;
import com.ey.digital.adc.aml.ci.service.AggregationService;
import com.ey.digital.adc.aml.ci.ui.request.AggregationUIRequest;
import com.ey.digital.adc.aml.ci.ui.request.TestResultsUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;

@RestController
@RequestMapping("/aggregation")
public class AggregationController extends AbstractController<AggregationUIRequest, TableSetting> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private final AggregationService aggregationService;
	
	public AggregationController(AggregationService aggregationService) {
		this.aggregationService = aggregationService;
	}
	
	@PostMapping(value = "/generate-table")
	public List<String> generateTable(@Valid @RequestBody AggregationUIRequest request) throws Exception {
		logger.info("Received token: " + request.getToken());
		Integer userId = validateToken(request.getToken());
		TableSetting tableSetting = request.getMessage();
		if (tableSetting == null) 
			throw new InvalidMessageException("Null/empty message received for generateTable.");
		return aggregationService.generateTable(tableSetting, userId, request.getToken());
	}
	
	@GetMapping(value = "/table-query", params = { "tableName", "token" })
	public List<String> tableQuery(@RequestParam("tableName") String tableName, @RequestParam("token") String token) throws Exception {
		validateToken(token);
		return aggregationService.getTableQuery(tableName);
	}
	
	@GetMapping(value = "/start-test", params = { "tableName", "testId", "token" })
	public int startTest(@RequestParam("tableName") String tableName, 
			@RequestParam("testId") int testId, 
			@RequestParam("token") String token) throws Exception {
		int userId = validateToken(token);
		return aggregationService.startTest(userId, tableName, testId, token);
	}
	
	@GetMapping(value = "/script-library", params = { "token" })
	public List<Script> getScriptList(@RequestParam("token") String token) throws Exception {
		validateToken(token);
		return aggregationService.getScriptList();
	}
	
	@PostMapping(value = "/save-test-results")
	public void completeTest(@Valid @RequestBody TestResultsUIRequest request) throws Exception {
		logger.info("Received token: " + request.getToken());
		Integer userId = validateToken(request.getToken());
		TestStatus testStatus = request.getMessage();
		if (testStatus == null) 
			throw new InvalidMessageException("Null/empty message received for completeTest.");
		aggregationService.completeTest(userId, testStatus);
	}
	
	@GetMapping(value = "/test-results", params = { "testExecutionResultsId", "token" })
	public TestExecutionResults getTestResults(@RequestParam("testExecutionResultsId") Integer testExecutionResultsId,
			@RequestParam("token") String token) throws Exception {
		validateToken(token);
		return aggregationService.getTestResults(testExecutionResultsId);
	}
}
