package com.ey.digital.adc.aml.ci.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name = "users")
@Data
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "userid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer ptUserId;
	
	@Column(name = "firstname")
	private String firstName;
	
	@Column(name = "lastname")
	private String lastName;
	
	@NotNull
	@Column(name = "loginid")
	private String email;
	@NotNull
	@Column(name = "loginpassword")
	private String password;
	
	@NotNull
	@Column(name = "clientid")
	private Integer clientId;

}
