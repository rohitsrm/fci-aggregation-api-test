package com.ey.digital.adc.aml.ci.service;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.ci.model.AggregationColumn;
import com.ey.digital.adc.aml.ci.model.Script;
import com.ey.digital.adc.aml.ci.model.TableQuery;
import com.ey.digital.adc.aml.ci.model.TableSetting;
import com.ey.digital.adc.aml.ci.model.TableSetting.AggregationTable;
import com.ey.digital.adc.aml.ci.model.TestExecutionResults;
import com.ey.digital.adc.aml.ci.model.TestStatus;
import com.ey.digital.adc.aml.ci.sql.server.repo.ScriptRepository;
import com.ey.digital.adc.aml.ci.sql.server.repo.TableQueryRepository;
import com.ey.digital.adc.aml.ci.sql.server.repo.TestExecutionResultsRepository;
import com.ey.digital.adc.aml.ci.oozie.rest.client.OozieClient;
import com.ey.digital.adc.core.utils.DateTimeUtil;

@Service
public class AggregationService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${hive.schema}")
	private String hiveSchema;
	
	@Autowired
	private final TableQueryRepository tableQueryRepository;
	
	@Autowired
	private final TestExecutionResultsRepository testExecutionResultsRepository;
	
	@Autowired
	private final ScriptRepository scriptRepository;
	
	@Autowired
	OozieClient oozieClient;
	
	@Autowired
	public AggregationService(TableQueryRepository tableQueryRepository,
			TestExecutionResultsRepository testExecutionResultsRepository,
			ScriptRepository scriptRepository) {
		this.tableQueryRepository = tableQueryRepository;
		this.testExecutionResultsRepository = testExecutionResultsRepository;
		this.scriptRepository = scriptRepository;
	}
	
	public void saveScript(int userId, String name, String category) {
		Script script = new Script();
		script.setName(name);
		script.setCategory(category);
		script.setStatus("Inactive");
		script.setCreatedBy(userId);
		script.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		script.setLastUpdatedBy(userId);
		script.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		scriptRepository.save(script);
	}
	
	private String getDuration(String shortDuration) {
		String duration = "";
		
		switch(shortDuration.toLowerCase()) {
		case "dd": 
			duration = "DAY(S)";
			break;
		case "mm":
			duration = "MONTH(S)";
			break;
		case "yy":
			duration = "YEAR(S)";
			break;
		}
		
		return duration;
	}
	
	private String getAggregation(String aggType) {
		String aggregation = "";
		switch(aggType.toLowerCase()) {
		case "amount": 
			aggregation = "sum(tran_amt)";
			break;
		case "quantity":
			aggregation = "count(tran_amt)";
			break;
		}
		return aggregation;
	}
	
	private String getStartDate(String endDate, int lookBack, String lookBackType) throws ParseException {
		//SimpleDateFormat sdfmt = new SimpleDateFormat("yyyy-MM-dd");
		DateTimeFormatter ldtfmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String startDate = "";
		
		switch(lookBackType.toLowerCase()) {
		case "dd": 
			startDate = ldtfmt.format(LocalDate.parse(endDate, ldtfmt).minusDays(lookBack));
			break;
		case "mm":
			startDate = ldtfmt.format(LocalDate.parse(endDate, ldtfmt).minusMonths(lookBack));
			break;
		case "yy":
			startDate = ldtfmt.format(LocalDate.parse(endDate, ldtfmt).minusYears(lookBack));
			break;
		}
		return startDate;
	}
	
	private AggregationColumn saveAggregationTableQuery(AggregationTable aggregationTable, int clientId, String tableName, int userId) throws ParseException {
		AggregationColumn aggColumn = new AggregationColumn();
		
		String aggTableName = hiveSchema + ".tran_agg_" + aggregationTable.getRunDate().replace("-", "") + "_" + 
				aggregationTable.getAggregationType().toLowerCase() + "_" + 
				String.valueOf(aggregationTable.getLookBackValue()) + aggregationTable.getLookBackType();
		
		aggColumn.setTableName(aggTableName);
		aggColumn.setTableShortName(aggregationTable.getAggregationType().toLowerCase() + 
				String.valueOf(aggregationTable.getLookBackValue()) + aggregationTable.getLookBackType().toLowerCase());
		aggColumn.setAggregationType(aggregationTable.getAggregationType().toLowerCase());
		aggColumn.setColumnValues(aggregationTable.getAggregationColumnValues());
		
		String query = "";
		String startDate = getStartDate(aggregationTable.getRunDate(), aggregationTable.getLookBackValue(), aggregationTable.getLookBackType());
		String endDate = aggregationTable.getRunDate();

		TableQuery tableQuery = new TableQuery();
		tableQuery.setClientId(clientId);
		tableQuery.setTableName(tableName);
		query = "DROP TABLE IF EXISTS " + aggTableName;
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);

		tableQuery = new TableQuery();
		tableQuery.setClientId(clientId);
		tableQuery.setTableName(tableName);
		query = "CREATE TABLE IF NOT EXISTS " + aggTableName + " (party_key bigint, tran_cd_desc string, agg_type string, interval_size int, " + 
				"interval_type string, start_date string, end_date string, value decimal(18,2))";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);

		tableQuery = new TableQuery();
		tableQuery.setClientId(clientId);
		tableQuery.setTableName(tableName);
		query = "INSERT INTO " + aggTableName + " SELECT party_key, tran_cd_desc, '" + aggregationTable.getAggregationType() + "', " + 
				String.valueOf(aggregationTable.getLookBackValue()) + ", '" + getDuration(aggregationTable.getLookBackType()) + "', '" + 
				startDate + "', '" + endDate + "', " + 
				getAggregation(aggregationTable.getAggregationType()) + " FROM " + hiveSchema + ".transaction WHERE tran_date BETWEEN '" + 
				startDate + "' AND '" + endDate+ "' " + "GROUP BY party_key,tran_cd_desc";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		
		return aggColumn;
	}
	
	public List<String> generateTable(TableSetting tableSetting, int userId, String token) throws IOException, ParseException {
		List<AggregationColumn> aggColumns = new ArrayList<AggregationColumn>();
		
		for(AggregationTable aggregationTable:  tableSetting.getAggregationTables())  {
			aggColumns.add(saveAggregationTableQuery(aggregationTable, tableSetting.getClientId(), tableSetting.getTableName(), userId));
		}
		
		List<String> results = getTableQuery(tableSetting.getTableName());
	
		String query = "";
		
		TableQuery tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ars");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".ars_modeling_dataset_1";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ars");
		query = "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".ars_modeling_dataset_1 AS SELECT p.*, hri.high_risk_indicator_key, " + 
				"hri.last_risk_scored_age, hri.latest_risk_level, hri.party_risk_score_rv, hri.address_rv, " + 
				"hri.foreign_corporation_rv, hri.foreign_individual_rv, hri.pep_rv, hri.embassies_frgn_consulates_rv, " + 
				"hri.sar_filed_rv, hri.cust_under_suspicion_rv, hri.cust_under_subpeona_rv, hri.mail_code_rv, " + 
				"hri.account_type_rv, hri.collateral_type_rv, hri.employee_status_rv, hri.high_risk_business_rv, " + 
				"hri.money_service_business_rv, hri.client_segment_rv, hri.channel_of_account_opening_rv, hri.occupation_rv, " + 
				"hri.initial_source_of_funds_rv, hri.inactive_account_rv, cast(round(rand()*100) % 11 as int) AS tot_prior_alerts, " + 
				"cast(round(rand()*100) % 2 as int) AS escalated_to_sar FROM " + hiveSchema + ".party p " + 
				"LEFT JOIN " + hiveSchema + ".high_risk_indicator hri ON p.party_key = hri.party_key";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ars");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".ars_modeling_dataset_2";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ars");
		query = "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".ars_modeling_dataset_2 AS SELECT amd1.*, apr.acct_key, " + 
				"a.acct_type_cd AS account_type, a.acct_status_cd AS account_classification, a.product_type_cd AS account_product, " + 
				"a.source_of_fund_cd AS account_channel, datediff(current_date(),a.acct_open_dt) AS max_days_acct_opened " + 
				"FROM " + hiveSchema + ".ars_modeling_dataset_1 amd1, " + hiveSchema + ".account_party_relation apr, " + 
				hiveSchema + ".account a WHERE amd1.party_key = apr.party_key AND apr.acct_key = a.acct_key";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ars");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".ars_modeling_dataset";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		query = "";
		query += "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".ars_modeling_dataset" + " AS ";
		query += "SELECT row_number() OVER (ORDER BY '1') AS agg_key, amd2.*, ";
		
		for(AggregationColumn aggColumn: aggColumns) {
			for(String columnValue: aggColumn.getColumnValues()) {
				query += "CASE WHEN " + aggColumn.getTableShortName() + ".agg_type = '" + aggColumn.getAggregationType() + "' AND " +
						aggColumn.getTableShortName() + ".tran_cd_desc = '" + columnValue + "' THEN " + aggColumn.getTableShortName() + 
						".value END AS " + aggColumn.getAggregationType() + "_" + columnValue  + "_" + aggColumn.getTableShortName() + ", ";
			}		
		}
		
		for(AggregationColumn aggColumn: aggColumns) {
			query += aggColumn.getTableShortName() + ".start_date AS " + aggColumn.getTableShortName() + "_start_date, ";
			query += aggColumn.getTableShortName() + ".end_date AS " + aggColumn.getTableShortName() + "_end_date, ";
		}
		
		query = query.substring(0, query.length() - 2);
		
		query += " FROM " + hiveSchema + ".ars_modeling_dataset_2 amd2 ";
		
		for(AggregationColumn aggColumn: aggColumns) {
			query += "LEFT JOIN " + aggColumn.getTableName() + " " + aggColumn.getTableShortName() +
					" ON amd2.party_key = " + aggColumn.getTableShortName() + ".party_key "; 
		}
		
		query += "WHERE amd2.party_key IN (SELECT party_key FROM (SELECT party_key FROM ";
		
		for(AggregationColumn aggColumn: aggColumns) {
			query += aggColumn.getTableName() + " UNION SELECT party_key FROM ";
		}
		
		query = query.substring(0, query.length() - 29);
		
		query += ") p)";
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ars");
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".ng_party_account";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".ng_party_account AS " + 
				"SELECT p.party_key, CONCAT(p.party_f_name,' ',p.party_m_name,' ',p.party_l_name) AS party_name, " + 
				"ap.acct_key AS account_key, a.acct_name AS account_name, a.additional_acct_id_1 AS account_other_name, " + 
				"p.party_type_cd AS party_type, CASE WHEN p.party_type_cd = UPPER('INDIVIDUAL') THEN 'SINGLE' ELSE 'JOINT' " + 
				"END AS is_single_joint, p.occupation, p.number_of_dependents as number_of_related_parties, " +
				"a.acct_type_cd as account_category_desc, a.acct_classification_desc as account_classification_desc, " +
				"a.product_type_cd as product_desc, a.sub_product_type_cd as sub_product_desc, a.acct_type_cd as account_type_desc, " + 
				"a.acct_status_cd as account_status_desc, a.acct_branch_cd as branch_name, a.sector_cd as sector_name, " + 
				"a.acct_open_dt as opened_date, pa.addr_line_1, pa.addr_line_2, pa.addr_line_3, pa.city, pa.state_province, " + 
				"pa.postal_cd, pa.addr_type_cd as address_relation_type_cd, e.email, ph.phone FROM " + hiveSchema + ".account a " + 
				"LEFT JOIN " + hiveSchema + ".account_party_relation ap on a.acct_key = ap.acct_key " + 
				"LEFT JOIN " + hiveSchema + ".party p on p.party_key = ap.party_key " + 
				"LEFT JOIN " + hiveSchema + ".party_address pa on p.party_key = pa.party_key " + 
				"LEFT JOIN " + hiveSchema + ".email_address e on p.party_key = e.party_key " + 
				"LEFT JOIN " + hiveSchema + ".phone_number ph on p.party_key = ph.party_key";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".ng_transaction_alerts";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".ng_transaction_alerts AS " + 
				"SELECT ac.acct_key AS account_key, ac.acct_name, ac.acct_id, ac.acct_type_cd, ac.net_worth, tr.tran_date, " + 
				"ac.country_cd AS opp_country_name, tr.tran_key, CASE " +
				"WHEN tr.credit_debit_indicator = 'credit' THEN tr.originator_account_number ELSE tr.beneficiary_account_number " + 
				"END opp_account_number, CASE WHEN tr.credit_debit_indicator = 'credit' THEN tr.originator_name ELSE tr.beneficiary_name " + 
				"END opp_account_name, tr.tran_cd_desc, tr.tran_desc, tr.tran_channel, tr.credit_debit_indicator, al.alert_key, " + 
				"al.alert_date, al.alert_type, al.alert_name, al.business_unit, al.alert_status, al.alert_owner, al.is_case, " + 
				"al.alert_last_updated_by, al.alert_created_date, al.alert_last_updated_date, al.alert_party_name, al.alert_account_name, " + 
				"al.alert_score, al.alert_negative_news_indicator, al.alert_negative_news_score, al.alert_negative_news_details, " + 
				"al.alert_score_details, al.execution_id, al.execution_date, al.ruleid, al.focal_entity, al.focus_key_1, " + 
				"al.focus_key_2, al.focus_key_3 FROM " + hiveSchema + ".account ac, " + hiveSchema + ".transaction tr, " + 
				hiveSchema + ".alert_input al WHERE ac.acct_key = tr.acct_key AND al.tran_key = tr.tran_key";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".ng_transaction_channels";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".ng_transaction_channels AS " + 
				"SELECT ac.acct_key AS account_key, ac.acct_name AS account_name, ac.acct_id AS account_number, " + 
				"ac.acct_type_cd AS sam_population_group_cd, tr.tran_amt AS tran_amt, tr.tran_date AS execution_local_date_time, " + 
				"tr.tran_key AS transaction_key, CASE WHEN tr.credit_debit_indicator = 'credit' THEN tr.originator_account_number " + 
				"ELSE tr.beneficiary_account_number END opp_account_number, CASE " +
				"WHEN tr.credit_debit_indicator = 'credit' THEN tr.originator_name ELSE tr.beneficiary_name END opp_account_name, " + 
				"tr.tran_cd AS transaction_code, tr.tran_cd_desc AS transaction_description, tr.tran_channel AS channel_desc, " + 
				"tr.credit_debit_indicator FROM " + hiveSchema + ".account ac, " + 
				hiveSchema + ".transaction tr WHERE ac.acct_key = tr.acct_key";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".ng_modeling_dataset";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_ng");
		query = "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".ng_modeling_dataset ROW format delimited fields terminated BY ',' AS " + 
				"SELECT pa.*, ta.acct_type_cd, ta.net_worth, ta.tran_date, ta.opp_country_name, ta.tran_key, ta.opp_account_number, " + 
				"ta.opp_account_name, ta.tran_cd_desc, ta.tran_desc, ta.tran_channel, ta.credit_debit_indicator, tc.account_number, " + 
				"tc.sam_population_group_cd, tc.tran_amt, tc.execution_local_date_time, tc.transaction_key, tc.transaction_code, " + 
				"tc.transaction_description, tc.channel_desc, ta.alert_key, ta.alert_date, ta.alert_type, ta.alert_name, " + 
				"ta.business_unit, ta.alert_status, ta.alert_owner, ta.is_case, ta.alert_last_updated_by, ta.alert_created_date, " + 
				"ta.alert_last_updated_date, ta.alert_party_name, ta.alert_account_name, ta.alert_score, ta.alert_negative_news_indicator, " + 
				"ta.alert_negative_news_score, ta.alert_negative_news_details, ta.alert_score_details, ta.execution_id, ta.execution_date, " + 
				"ta.ruleid, ta.focal_entity, ta.focus_key_1, ta.focus_key_2, ta.focus_key_3 FROM " + hiveSchema + ".ng_party_account pa " + 
				"LEFT JOIN " + hiveSchema + ".ng_transaction_channels tc ON pa.account_key = tc.account_key " + 
				"LEFT JOIN " + hiveSchema + ".ng_transaction_alerts ta ON pa.account_key = ta.account_key";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_nn");
		query = "DROP TABLE IF EXISTS " + hiveSchema + ".nn_modeling_dataset";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		tableQuery = new TableQuery();
		tableQuery.setClientId(tableSetting.getClientId());
		tableQuery.setTableName(tableSetting.getTableName() + "_nn");
		query = "CREATE TABLE IF NOT EXISTS " + hiveSchema + ".nn_modeling_dataset ROW format delimited fields terminated BY ',' AS " + 
				"SELECT p.party_key, p.party_f_name, p.party_m_name, p.party_l_name, pa.city, pa.state_province, pa.country_cd, " + 
				"p.occupation, p.employer FROM " + hiveSchema + ".party p " + 
				"LEFT JOIN " + hiveSchema + ".party_address pa ON p.party_key = pa.party_key";
		tableQuery.setQuery(query);
		tableQuery.setCreatedBy(userId);
		tableQuery.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQuery.setLastUpdatedBy(userId);
		tableQuery.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		tableQueryRepository.save(tableQuery);
		results.add(query);
		
		oozieClient.startAGG(tableSetting.getTableName(), token);
		
		return results;
	}
	
	public List<String> getTableQuery(String tableName) {
		List<TableQuery> tableQueryList = tableQueryRepository.findAllByTableName(tableName);
		return tableQueryList.stream().map(TableQuery::getQuery).collect(Collectors.toList());
	}
	
	public int startTest(int userId, String tableName, int testId, String token) throws IOException {
		TestExecutionResults testExecutionResultsData = new TestExecutionResults();
		testExecutionResultsData.setTestId(testId);
		testExecutionResultsData.setStatus("STARTED");
		testExecutionResultsData.setExecutionStartTime(DateTimeUtil.getCurrentDateTime());
		testExecutionResultsData.setCreatedBy(userId);
		testExecutionResultsData.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		testExecutionResultsData.setLastUpdatedBy(userId);
		testExecutionResultsData.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		testExecutionResultsData = testExecutionResultsRepository.save(testExecutionResultsData);
		
		oozieClient.startTest(tableName, testId, token);
		
		return testExecutionResultsData.getTestExecutionResultsId();
	}
	
	public List<Script> getScriptList() {
		return scriptRepository.findAll();
	}
	
	public void completeTest(int userId, TestStatus testStatus) {
		logger.info("completeTest for testExecutionResultsId: " + String.valueOf(testStatus.getTestExecutionResultsId()));
		TestExecutionResults testExecutionResultsData = testExecutionResultsRepository.getOne(testStatus.getTestExecutionResultsId());
		testExecutionResultsData.setStatus("COMPLETED");
		testExecutionResultsData.setResultLocation(testStatus.getResultLocation());
		testExecutionResultsData.setExecutionEndTime(DateTimeUtil.getCurrentDateTime());
		testExecutionResultsData.setCreatedBy(userId);
		testExecutionResultsData.setCreatedDateTime(DateTimeUtil.getCurrentDateTime());
		testExecutionResultsData.setLastUpdatedBy(userId);
		testExecutionResultsData.setLastUpdatedDateTime(DateTimeUtil.getCurrentDateTime());
		testExecutionResultsRepository.save(testExecutionResultsData);
	}
	
	public TestExecutionResults getTestResults(int testExecutionResultsId) {
		return testExecutionResultsRepository.getOne(testExecutionResultsId);
	}
}
