package com.ey.digital.adc.aml.ci.sql.server.repo;

import com.ey.digital.adc.aml.ci.model.Script;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface ScriptRepository extends ISQLRepository<Script, Integer> {

}
