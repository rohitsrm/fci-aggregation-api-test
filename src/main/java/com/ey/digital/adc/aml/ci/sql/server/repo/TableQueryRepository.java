package com.ey.digital.adc.aml.ci.sql.server.repo;

import java.util.List;

import com.ey.digital.adc.aml.ci.model.TableQuery;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface TableQueryRepository extends ISQLRepository<TableQuery, Integer>{
	List<TableQuery> findAllByTableName(String tableName);
}
