package com.ey.digital.adc.aml.ci.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class TestStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int testExecutionResultsId;
	private String resultLocation;
}
