package com.ey.digital.adc.aml.ci.sql.server.repo;

import com.ey.digital.adc.aml.ci.model.TestExecutionResults;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface TestExecutionResultsRepository extends ISQLRepository<TestExecutionResults, Integer> {

}
