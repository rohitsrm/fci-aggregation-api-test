package com.ey.digital.adc.aml.ci.ui.request;

import com.ey.digital.adc.aml.ci.model.TestStatus;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public class TestResultsUIRequest extends AbstractUIRequest<TestStatus> {
	private static final long serialVersionUID = 1L;

}
