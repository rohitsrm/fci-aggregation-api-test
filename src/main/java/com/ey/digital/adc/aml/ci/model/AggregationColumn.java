package com.ey.digital.adc.aml.ci.model;

import java.util.List;

import lombok.Data;

@Data
public class AggregationColumn {
	private String tableName;
	private String tableShortName;
	private String aggregationType;
	private String[] columnValues;
}
