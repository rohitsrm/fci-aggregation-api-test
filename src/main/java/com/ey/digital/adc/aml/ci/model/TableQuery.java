package com.ey.digital.adc.aml.ci.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "tablequery")
@Data
@EqualsAndHashCode(callSuper=false)
public class TableQuery extends Audit {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "tablequeryid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer tableQueryId;
	
	@Column(name = "clientid")
	protected Integer clientId;

	@Column(name = "tablename")
	protected String tableName;
	
	@Column(name = "query")
	protected String query;
}
