package com.ey.digital.adc.aml.ci.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.ey.digital.adc.core.model.Audit;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "testexecutionresults")
public class TestExecutionResults extends Audit {
	private static final long serialVersionUID = -3658711337504693742L;

	@Id
	@Column(name = "testexecutionresultsid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer testExecutionResultsId;
	
	@Column(name = "testid")
	protected Integer testId;
	
	@Column(name = "executionstarttime")
	protected String executionStartTime;
	
	@Column(name = "executionendtime")
	protected String executionEndTime;
	
	@Column(name = "status")
	protected String status;
	
	@Column(name = "resultlocation")
	protected String resultLocation;
}
