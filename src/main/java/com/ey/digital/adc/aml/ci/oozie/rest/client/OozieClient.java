package com.ey.digital.adc.aml.ci.oozie.rest.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ey.digital.adc.aml.ci.model.OozieJobStartRestResponse;
import com.ey.digital.adc.aml.ci.model.RestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
@Data
public class OozieClient {
private final Logger logger = Logger.getLogger(OozieClient.class);
    HashMap<String, String> oozieConf;
    Properties prop = new Properties();

    public OozieClient() {
    }

    String oozieStartUrl;
    String oozieHostname;
    String oozieProtocol;
    String ooziePort;
    String oozieContentType;
    String ooziePassword;
    String oozieUsername;
    String oozieAGGWorkflowFolder;
    String oozieARSWorkflowFolder;
    String oozieNGWorkflowFolder;
    String oozieNNWorkflowFolder;
    String aggregationAPI;
    String httpProxyUrl;
    	boolean useNetworkProxy;

    @PostConstruct
    public void check() throws JsonProcessingException {
        logger.info("oozieStartUrl = " + oozieStartUrl);
        logger.info("props oozie = " + oozieConf);
    }


    private String serialize(Map<String, String> map) {
        StringBuilder payload = new StringBuilder("<configuration>");
        map.forEach((key, value) -> {
            payload.append("<property><name>");
            payload.append(key);
            payload.append("</name><value>");
            payload.append(value);
            payload.append("</value></property>");

        });
        String s = payload.append("</configuration>").toString();
        System.out.println("s = " + s);
        return s;
    }

    public RestResponse oozieRestClientPost(String payload) throws IOException {
        return execute(RequestMethod.POST, payload);
    }


    public RestResponse oozieRestClientGet() throws IOException {
        return execute(RequestMethod.GET, null);
    }

    public RestResponse execute(RequestMethod requestMethod, String payload) throws IOException {
        RestResponse restResp = new RestResponse();
        HttpHost target = new HttpHost(oozieHostname, Integer.parseInt(ooziePort), oozieProtocol);
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope(target.getHostName(), target.getPort()),
                new UsernamePasswordCredentials(oozieUsername, ooziePassword));
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
        HttpHost httpProxy = new HttpHost(httpProxyUrl, 8080, "http");
        RequestConfig config = RequestConfig.custom().setProxy(httpProxy).build();
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(target, basicAuth);
        HttpClientContext localContext = HttpClientContext.create();
        localContext.setAuthCache(authCache);
        CloseableHttpResponse response = null;
        ObjectMapper mapper = new ObjectMapper();
        switch (requestMethod) {
            case GET:
                HttpGet getRequest = new HttpGet(oozieStartUrl);
                logger.info("Executing request " + getRequest.getRequestLine() + " to target " + target);
                response = httpclient.execute(target, getRequest, localContext);
                break;
            case POST:
                HttpPost postRequest = new HttpPost(oozieStartUrl);
                StringEntity input = new StringEntity(payload);
                input.setContentType(oozieContentType);
                postRequest.setEntity(input);
                if (useNetworkProxy) postRequest.setConfig(config);
                response = httpclient.execute(target, postRequest, localContext);

                String content = EntityUtils.toString(response.getEntity());
                OozieJobStartRestResponse job = mapper.readValue(content, OozieJobStartRestResponse.class);
                restResp.setRespBody(job.getId());
                System.out.println("restResp = " + restResp.getRespBody());
                break;
            case PUT:
                HttpPut put = new HttpPut(oozieStartUrl);
                StringEntity input1 = new StringEntity(payload);
                input1.setContentType(oozieContentType);
                put.setEntity(input1);
                logger.info("Executing request " + put.getRequestLine() + " to target " + target);
                response = httpclient.execute(target, put, localContext);
		case DELETE:
			break;
		case HEAD:
			break;
		case OPTIONS:
			break;
		case PATCH:
			break;
		case TRACE:
			break;
		default:
			break;
        }
        httpclient.close();
        return restResp;
    }

    public String startAGG(String tableName, String token) throws IOException {
        HashMap<String, String> map = new HashMap<>(this.oozieConf);
        map.put("oozie.wf.application.path", oozieAGGWorkflowFolder);
        map.put("url", aggregationAPI);
        map.put("tableName", tableName);
        map.put("token", token);
        return execute(RequestMethod.POST, serialize(map)).getRespBody();
    }
    
    public String startTest(String tableName, int testId, String token) throws IOException {
        HashMap<String, String> map = new HashMap<>(this.oozieConf);
        
        switch(testId) {
        	case 1:
        		map.put("oozie.wf.application.path", oozieARSWorkflowFolder);
        		break;
        	case 2:
        		map.put("oozie.wf.application.path", oozieNNWorkflowFolder);
        		break;
        	case 3:
        		map.put("oozie.wf.application.path", oozieNGWorkflowFolder);
        		break;
        }
        		
        map.put("url", aggregationAPI);
        map.put("tableName", tableName);
        map.put("token", token);
        return execute(RequestMethod.POST, serialize(map)).getRespBody();
    }
}
