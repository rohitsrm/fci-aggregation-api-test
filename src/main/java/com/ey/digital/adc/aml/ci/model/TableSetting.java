package com.ey.digital.adc.aml.ci.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TableSetting implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int clientId;
	private String tableName;
	private List<AggregationTable> aggregationTables = new ArrayList<>();
	
	@Data
	public static class AggregationTable {
		private String aggregationType;
		private String[] aggregationColumnValues;
		private int lookBackValue;
		private String lookBackType;
		private String runDate;
	}
}
