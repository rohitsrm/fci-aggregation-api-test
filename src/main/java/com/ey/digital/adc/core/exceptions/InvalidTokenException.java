package com.ey.digital.adc.core.exceptions;

public class InvalidTokenException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidTokenException() {
		this("Invalid Token received");
	}

	public InvalidTokenException(String message) {
		super(message);
	}
}
