package com.ey.digital.adc.core.controllers;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.sql.server.utils.SqlServerUtils;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public abstract class AbstractController<T extends AbstractUIRequest<V>, V extends Serializable> {
    private final String controllerName = this.getClass().getSimpleName().replaceAll("Controller", "");
    
    @Autowired
    SqlServerUtils sqlServerUtils;

    protected int validateToken(String token) throws InvalidTokenException {
        if (token == null || "".equals(token.trim())) {
            throw new InvalidTokenException();
        }
        final int userId = sqlServerUtils.validateToken(token);
        if (userId <= 0) {
            throw new InvalidTokenException();
        }
        return userId;
    }

    protected String getControllerName() {
        return controllerName;
    }
}
