FROM openjdk:8-jre-alpine
LABEL maintainer="jibin.babu@ey.com"
COPY ./target/fci-aggregation-*.jar /agg_api.jar
CMD ["/usr/bin/java", "-Dspring.profiles.active=${SPRING_PROFILES_ACTIVE:=local}", "-jar", "/agg_api.jar"]
EXPOSE 8080
